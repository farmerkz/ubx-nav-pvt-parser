#include <Arduino.h>

#ifndef __restoredefaults_H__
#define __restoredefaults_H__

extern void sendPacket(Stream *s, byte *packet, uint16_t len);

// Send a packet to the receiver to restore default configuration (BBR/Flash)
void restoreDefaults(Stream *s)
{
  // CFG-CFG packet
  byte packet[] PROGMEM = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x09, // id
      0x0D, // length
      0x00, // length
      0xFF, // payload
      0xFF, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0x00, // payload
      0xFF, // payload
      0xFF, // payload
      0x00, // payload
      0x00, // payload
      0x03, // payload
      0x1B, // CK_A
      0x9A, // CK_B
  };

  sendPacket(s, packet, sizeof(packet));
}
#endif
