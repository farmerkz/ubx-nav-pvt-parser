#include <Arduino.h>

#ifndef __changefixmode_H__
#define __changefixmode_H__

extern void sendPacket(Stream *s, byte *packet, uint16_t len);

// Send a packet to the receiver to change Position Fixing Mode
void changeFixMode(Stream *s, uint8_t fixmode)
{
  // CFG-NAV5 packet
  byte packet[44] = {
      0xB5, // sync char 1
      0x62, // sync char 2
      0x06, // class
      0x24, // id
      0x24, // length
      0x00, // length
      0x04, // payload - choose fixing mode
      0x00,
      0x00,
      0x00 // payload - fixMode
  };

  uint8_t *fxm = (uint8_t *)&packet[9];
  *fxm = fixmode;

  byte packetSize = sizeof(packet);

  packet[packetSize - 2] = 0;
  packet[packetSize - 1] = 0;

  for (byte j = 0; j < packetSize - 4; j++)
  {
    packet[packetSize - 2] += packet[2 + j];
    packet[packetSize - 1] += packet[packetSize - 2];
  }

  sendPacket(s, packet, sizeof(packet));
}
#endif
