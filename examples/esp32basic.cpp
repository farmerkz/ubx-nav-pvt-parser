/*
  This a simple example of using the library on an ESP32.
*/

#include <Arduino.h>
#include "ubx-nav-pvt-parser.h"
#include "changebaudrate.h"
#include "disablenmea.h"
#include "enablenavpvt.h"
#include "Stream.h"

HardwareSerial gpsSerial(2);

void setup()
{
  Serial.begin(115200);
  gpsSerial.begin(9600, SERIAL_8N1, 16, 17);
  delay(100);
  changeBaudrate(&gpsSerial, 115200);
  delay(100);
  gpsSerial.begin(115200, SERIAL_8N1, 16, 17);
  delay(100);
  gpsSerial.flush();
  delay(100);
  disableNmea(&gpsSerial);
  delay(100);
  enableNavPvt(&gpsSerial);
  delay(100);
}

void loop()
{
  if (processGPS(&gpsSerial))
  {
    Serial.printf("---------->\nВремя: %02d:%02d:%02d\n3D, Флаги: %2X, FixType=%2i, Nsat=%2i, lat=%2.5f, lon=%2.5f, h=%.2f, pDOP=%.2f\n",
                  pvt.hour,
                  pvt.minute,
                  pvt.second,
                  pvt.flags,
                  pvt.fixType,
                  pvt.numSV,
                  pvt.lat * en7,
                  pvt.lon * en7,
                  pvt.height * mm2m,
                  pvt.pDOP * 0.01);
    Serial.printf("Остаток свободной памяти: %d\n", ESP.getFreeHeap());
  };
}
