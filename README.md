# UBX-NAV-PVT Simple Parser

A library for parsing UBX-NAV-PVT u-blox GNSS receivers.

## Background

This is a simplified version of the library https://gitlab.com/farmerkz/u-blox-arduino for parsing UBX-NAV-PVT u-blox M8 series of GNSS (“GPS”) receiver modules. It uses the u-blox UBX binary protocol exclusively and only one type of message: UBX-NAV-PVT
