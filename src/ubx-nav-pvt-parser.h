/******************************************************************************
ubx-nav-pvt-parser.h
A simple parser UBX-NAV-PVT message from u-blox M8 GNSS receiver modules.
Andrey Zolotnitskiy
Jul 09 2020
https://gitlab.com/farmerkz/u-blox-arduino


Distributed as-is; no warranty is given.
******************************************************************************/

#ifndef _ubxNavPvt_
#define _ubxNavPvt_

#include <Arduino.h>

const double mm2m = 1.0e-3;
const double en7 = 1.0e-7;
const double en5 = 1.0e-5;

// FIX Flags
typedef enum
{
    NO_FIX,
    DEAD_RECKONING,
    FIX_2D,
    FIX_3D,
    GNSS_AND_DEAD_RECKONING,
    TIME_ONLY
    //...
} fixType_t;

// Dynamic model
typedef enum
{
    PORTABLE = 0,
    STATIONARY = 2,
    PEDESTRAIN,
    AUTOMOTIVE,
    SEA,
    AIRBORNE_1G,
    AIRBORNE_2G,
    AIRBORNE_4G,
    WRIST_WORN_WATCH,
    BIKE
} dynModel_t;

typedef enum
{
    F_2D_ONLY = 1,
    F_3D_ONLY,
    F_AUTO_2D_3D
} fixMode_t;

const unsigned char UBX_HEADER[] = {0xB5, 0x62};

struct NAV_PVT
{
    unsigned char cls;
    unsigned char id;
    unsigned short len;
    unsigned long iTOW;         // GPS time of week of the navigation epoch (ms)
    unsigned short year;        // Year (UTC)
    unsigned char month;        // Month, range 1..12 (UTC)
    unsigned char day;          // Day of month, range 1..31 (UTC)
    unsigned char hour;         // Hour of day, range 0..23 (UTC)
    unsigned char minute;       // Minute of hour, range 0..59 (UTC)
    unsigned char second;       // Seconds of minute, range 0..60 (UTC)
    unsigned char valid;        // Validity Flags (see graphic below)
    unsigned long tAcc;         // Time accuracy estimate (UTC) (ns)
    long nano;                  // Fraction of second, range -1e9 .. 1e9 (UTC) (ns)
    unsigned char fixType;      // GNSSfix Type, range 0..5
    unsigned char flags;        // Fix Status Flags
    unsigned char flags2;       // reserved
    unsigned char numSV;        // Number of satellites used in Nav Solution
    long lon;                   // Longitude (deg)
    long lat;                   // Latitude (deg)
    long height;                // Height above Ellipsoid (mm)
    long hMSL;                  // Height above mean sea level (mm)
    unsigned long hAcc;         // Horizontal Accuracy Estimate (mm)
    unsigned long vAcc;         // Vertical Accuracy Estimate (mm)
    long velN;                  // NED north velocity (mm/s)
    long velE;                  // NED east velocity (mm/s)
    long velD;                  // NED down velocity (mm/s)
    long gSpeed;                // Ground Speed (2-D) (mm/s)
    long heading;               // Heading of motion 2-D (deg)
    unsigned long sAcc;         // Speed Accuracy Estimate (mm/s)
    unsigned long headingAcc;   // Heading Accuracy Estimate (deg)
    unsigned short pDOP;        // Position dilution of precision
    unsigned char reserved1[6]; //Reserved
    long headVeh;               // Heading of vehicle (2-D) (deg)
    short magDec;               // Magnetic declination (deg)
    unsigned short magAcc;      // Magnetic declination accuracy (deg)
};

NAV_PVT pvt;

// Подсчет контрольной суммы пакета
// Возвращает контрольную сумму в массиве из двух байт
void calcChecksum(unsigned char *CK)
{
    memset(CK, 0, 2);
    for (int i = 0; i < (int)sizeof(NAV_PVT); i++)
    {
        CK[0] += ((unsigned char *)(&pvt))[i];
        CK[1] += CK[0];
    }
} // calcChecksum()

// Обработка данных с GNSS
// Параметр: поток, с которого поступают данные
// Возвращает:
//  true, если принят полный пакет с правильной контрольной суммой
//  falce, если пакет еще не принят полностью, либо неверная контрольная сумма
// Результат в структуре pvt
bool processGPS(Stream *s)
{
    static int fpos = 0;
    static unsigned char checksum[2];
    const int payloadSize = sizeof(NAV_PVT);
    while (s->available())
    {
        byte c = s->read();
        if (fpos < 2)
        {
            if (c == UBX_HEADER[fpos])
                fpos++;
            else
                fpos = 0;
        }
        else
        {
            if ((fpos - 2) < payloadSize)
                ((unsigned char *)(&pvt))[fpos - 2] = c;

            fpos++;

            if (fpos == (payloadSize + 2))
            {
                calcChecksum(checksum);
            }
            else if (fpos == (payloadSize + 3))
            {
                if (c != checksum[0])
                    fpos = 0;
            }
            else if (fpos == (payloadSize + 4))
            {
                fpos = 0;
                if (c == checksum[1])
                {
                    return true;
                }
            }
            else if (fpos > (payloadSize + 4))
            {
                fpos = 0;
            }
        }
    }
    return false;
} // processGPS()

void sendPacket(Stream *s, byte *packet, uint16_t len)
{
    for (byte i = 0; i < len; i++)
    {
        s->write(packet[i]);
    }
}

#endif
